#include "Smart_Beehive_hardware_config.hpp"
#include "Smart_Beehive_Mic_Sensor.hpp"

Smart_Beehive_Mic_Sensor::Smart_Beehive_Mic_Sensor(const char* Sensor_ID, int Sensor_Pin,const char* Server_Endpoint)
:Smart_Beehive_Sensors(Sensor_ID,Server_Endpoint),sensorPin(Sensor_Pin),dB_Level(0.0)
{
    pinMode(sensorPin,INPUT);
}

void Smart_Beehive_Mic_Sensor::Read_Sensor_data()
{
    default_API_t   create_json;
    unsigned long startMillis = millis();   // Start of sample window
    float peakToPeak = 0;   
    unsigned int signalMax = 0;             // Minimum value
    unsigned int signalMin = 1024;          // Maximum value (for ESP32 12-bit ADC)

    // Collect data for 50 ms
    while (millis() - startMillis < 50)
    {
        unsigned int sample = analogRead(sensorPin);  // Read analog data from the sensor pin
        if (sample < 1024)
        {
            if (sample > signalMax)
            {
                signalMax = sample;  // Save max levels
            }
            else if (sample < signalMin)
            {
                signalMin = sample;  // Save min levels
            }
        }
    }

    // Calculate peak-to-peak amplitude and convert to decibels
    peakToPeak = signalMax - signalMin;
    this->dB_Level = map(peakToPeak, 20, 900, 49.5, 90);

    // // Log the result
    // Serial.print("dB Level: ");
    // Serial.println(this->dB_Level);

    this->json_data.clear();
    create_json.apiKey=CONFIG_API_KEY; //config file
    create_json.hiveId=CONFIG_HIVE_ID; //config file
    create_json.sensorData=std::to_string(this->dB_Level);
    create_json.sensorEnum=CONFIG_SENSOR_ENUM;//config file --modify to hiveEnum
    create_json.sensorName=this->Sensor_ID; //const on the type of the sensor
    this->json_data = createJson(create_json);
}
