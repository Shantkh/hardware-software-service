#include "Smart_Beehive_GPS_Sensor.hpp"
#include "Smart_Beehive_hardware_config.hpp"

#define CONFIG_DEFAULT_LATITUDE 0
#define CONFIG_DEFAULT_LONGITUDE 0



Smart_Beehive_GPS_Sensor::Smart_Beehive_GPS_Sensor(const char* Sensor_ID, HardwareSerial &serialPort, const char* Server_Endpoint)
    : Smart_Beehive_Sensors(Sensor_ID, Server_Endpoint), gpsSerial(serialPort)
{
    gpsSerial.begin(9600); // Start serial communication with the GPS module at 9600 baud rate
}

void Smart_Beehive_GPS_Sensor::Read_Sensor_data()
{
    GPS_API_t create_json;
    
    // Check if GPS data is available and process it
    while (gpsSerial.available() > 0)
    {
        if (gps.encode(gpsSerial.read())) // Encode GPS data from the serial input
        {
            // If GPS location is valid, store latitude and longitude
            if (gps.location.isValid())
            {
                this->latitude = gps.location.lat();
                this->longitude = gps.location.lng();
            }
            else
            {
                Serial.println("GPS data is invalid");
                this->latitude = CONFIG_DEFAULT_LATITUDE;  // Use default latitude if data is invalid
                this->longitude = CONFIG_DEFAULT_LONGITUDE; // Use default longitude if data is invalid
            }
        }
    }

    // Prepare JSON data for the API
    this->json_data.clear();
    create_json.apiKey = CONFIG_API_KEY;  // Fetch API key from the configuration
    create_json.hiveId = CONFIG_HIVE_ID;  // Fetch hive ID from the configuration
    
    create_json.locationLatitude =std::to_string(this->latitude);
    create_json.locationLongitude =std::to_string(this->longitude);
    create_json.locationLatitudeDegree ="0";
    create_json.locationLongitudeDegree ="0";
    create_json.gpsAltitude="0";
    create_json.gpsAngle="o";
    
    create_json.sensorEnum = CONFIG_SENSOR_ENUM; // Sensor type identifier
    create_json.sensorName = this->Sensor_ID;     // The sensor ID for the GPS sensor
    this->json_data = createJson(create_json); // Create JSON data for transmission
}

StaticJsonDocument<JSON_BUFFER_SIZE> Smart_Beehive_GPS_Sensor::createJson(GPS_API_t API_Data)
{
    StaticJsonDocument<JSON_BUFFER_SIZE> jsonDocument;
    #define member(type, name) jsonDocument[#name] = API_Data.name;
    GPS_Json_data
    #undef member
    return jsonDocument;
}

