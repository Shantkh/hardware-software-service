#include<string>
#include "Smart_Beehive_hardware_config.hpp"
#include"Smart_Beehive_Humidity_Sensor.hpp"


Smart_Beehive_Humidity_Sensor::Smart_Beehive_Humidity_Sensor(const char* Sensor_ID, uint16_t DHT_PIN, uint8_t DHT_TYPE,const char* Server_Endpoint)
:Smart_Beehive_Sensors(Sensor_ID,Server_Endpoint),DHT(DHT_PIN,DHT_TYPE)
{
    begin();
}


void Smart_Beehive_Humidity_Sensor::Read_Sensor_data()
{
    default_API_t   create_json;
    this->HumidityRead = readHumidity();
    if (isnan(this->HumidityRead))
    {
        Serial.print("Failed to read Humidity from DHT sensor_ID: ");
        Serial.println(this->Sensor_ID);
        this->HumidityRead = CONFIG_DEFAULT_SENSOR_VALUE;
    }
    this->json_data.clear();
    create_json.apiKey=CONFIG_API_KEY; //config file
    create_json.hiveId=CONFIG_HIVE_ID; //config file
    create_json.sensorData=std::to_string(HumidityRead);
    create_json.sensorEnum=CONFIG_SENSOR_ENUM;//config file --modify to hiveEnum
    create_json.sensorName=this->Sensor_ID; //const on the type of the sensor
    this->json_data = createJson(create_json);
}