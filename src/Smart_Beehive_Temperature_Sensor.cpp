#include <string>
#include "Smart_Beehive_hardware_config.hpp"
#include "Smart_Beehive_Temperature_Sensor.hpp"

Smart_Beehive_Temperature_Sensor::
    Smart_Beehive_Temperature_Sensor(const char* Sensor_ID, uint16_t One_wire_bus_pin, const char* Server_Endpoint)
    : Smart_Beehive_Sensors(Sensor_ID, Server_Endpoint), DallasTemperature(&oneWire),DHT(0,0)
{
    this->Sensor_type=Dallas_Sensor;
    oneWire.begin(One_wire_bus_pin);
    DallasTemperature::begin();
}


Smart_Beehive_Temperature_Sensor::
    Smart_Beehive_Temperature_Sensor(const char* Sensor_ID,uint16_t DHT_PIN,uint8_t DHT_TYPE, const char* Server_Endpoint)
    : Smart_Beehive_Sensors(Sensor_ID, Server_Endpoint), DHT(DHT_PIN,DHT_TYPE)
{
    this->Sensor_type=DHT_Sensor;
    DHT::begin();
}   




void Smart_Beehive_Temperature_Sensor::Read_Sensor_data()
{
    default_API_t   create_json;
    if (Sensor_type == DHT_Sensor)
    {
        this->temperatureRead = readTemperature();
        if (isnan(this->temperatureRead))
        {
            Serial.print("Failed to read temperatureRead from DHT sensor_ID: ");
            Serial.println(this->Sensor_ID);
            this->temperatureRead = CONFIG_DEFAULT_SENSOR_VALUE;
        }
    }
    else
    {
        // Request temperature from DS18B20 sensor
        requestTemperatures();
        this->temperatureRead = getTempCByIndex(0);
        if (this->temperatureRead == DEVICE_DISCONNECTED_C)
        {
            Serial.print("Failed to read temperature from DS18B20 sensor_ID: ");
            Serial.println(this->Sensor_ID);
            this->temperatureRead = CONFIG_DEFAULT_SENSOR_VALUE;
        }
    }
    this->json_data.clear();
    create_json.apiKey= CONFIG_API_KEY;         //config file
    create_json.sensorName= this->Sensor_ID;                                   //const on the type of the sensor
    create_json.hiveId= CONFIG_HIVE_ID;         //config file
    create_json.sensorEnum= CONFIG_SENSOR_ENUM;                         //config file --modify to hiveEnum
    create_json.sensorData= std::to_string(temperatureRead);
    
    this->json_data = createJson(create_json);
    }
