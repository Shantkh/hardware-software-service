#include "Smart_Beehive_Smoke_Sensor.hpp"
#include "Smart_Beehive_Config.hpp"  // Configuration file for constants

Smart_Beehive_Smoke_Sensor::Smart_Beehive_Smoke_Sensor(const char* Sensor_ID, int Sensor_Pin, const char* Server_Endpoint)
    : Smart_Beehive_Sensors(Sensor_ID, Server_Endpoint), sensorPin(Sensor_Pin)
{
    pinMode(sensorPin, INPUT);  // Set the smoke sensor pin as input
}

void Smart_Beehive_Smoke_Sensor::Read_Sensor_data()
{
    API_t create_json;

    // Read smoke level from the sensor pin (analog sensor reading)
    this->smokeLevel = analogRead(sensorPin);

    // Handle invalid or unexpected readings
    if (this->smokeLevel < 0)
    {
        Serial.print("Failed to read smoke sensor data for sensor ID: ");
        Serial.println(this->Sensor_ID);
        this->smokeLevel = CONFIG_DEFAULT_SENSOR_VALUE; // Use a default value if the reading fails
    }

    // Prepare JSON data for the API
    this->json_data.clear();
    create_json.apiKey = CONFIG_API_KEY;    // Fetch API key from config
    create_json.hiveId = CONFIG_HIVE_ID;    // Fetch hive ID from config
    create_json.sensorEnum = CONFIG_SENSOR_ENUM; // Sensor type (e.g., SMOKE_SENSOR)
    create_json.sensorId = this->Sensor_ID; // The sensor ID for the smoke sensor

    this->json_data = createJson(create_json);  // Create JSON for transmission

}
