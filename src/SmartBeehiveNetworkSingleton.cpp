#include "SmartBeehiveNetworkSingleton.hpp"

// Constructor for the Singleton class.
// Initializes the serial communication at a baud rate of 115200.
SmartBeehiveNetworkSingleton::SmartBeehiveNetworkSingleton()
{
}

// Static method to get the single instance of the class.
// This method ensures that only one instance of the class exists.
SmartBeehiveNetworkSingleton& SmartBeehiveNetworkSingleton::Get_Instance()
{
    static SmartBeehiveNetworkSingleton instance;
    return instance;
}


// Method to connect to a WiFi network.
// Parameters:
//   - WIFI_ssid: The SSID of the WiFi network.
//   - WIFI_password: The password of the WiFi network.
bool SmartBeehiveNetworkSingleton::Connect_Wifi(const char* ssid, const char* WIFI_password)
{
    if (ssid == nullptr || WIFI_password == nullptr) {
        Serial.println("Error: SSID or Password is null.");
        return false;
    }

    this->WiFi_ssid = ssid;
    this->WiFi_password = WIFI_password;

    WIFI.begin(ssid, WIFI_password);
    Serial.print("Connecting to WiFi");

    int8_t counter = 0;
    while (WIFI.status() != WL_CONNECTED && counter < 10) {
        delay(1000);
        Serial.print(".");
        counter++;
    }

    if (WIFI.status() == WL_CONNECTED) {
        Serial.print("\nWiFi connected: IP: ");
        Serial.println(WiFi.localIP());
        return true;
    } else {
        Serial.println("\nError: Failed to connect to WiFi.");
        return false;
    }
}

// Method to connect to a server over WiFi.
// Parameters:
//   - serverAddress: The address of the server.
//   - serverPort: The port number of the server.
bool SmartBeehiveNetworkSingleton::Connect_Server(const char* serverAddress, uint16_t serverPort)
{
    if (serverAddress == nullptr || serverPort == 0) {
        Serial.println("Error: Invalid server address or port.");
        return false;
    }

    this->serverAddress = serverAddress;
    this->serverPort = serverPort;

    if (WiFi_Client.connect(serverAddress, serverPort)) {
        Serial.println("Connected to the server.");
        return true;
    } else {
        Serial.println("Error: Failed to connect to the server.");
        return false;
    }
}

bool SmartBeehiveNetworkSingleton::Connect_Server()
{
    if (serverAddress == nullptr || serverPort == 0) {
        Serial.println("Error: Server parameters not set.");
        return false;
    }

    if (!WiFi_Client.connected()) {
        if (WiFi_Client.connect(serverAddress, serverPort)) {
            Serial.println("Connected to the server.");
            return true;
        } else {
            Serial.println("Error: Failed to connect to the server.");
            return false;
        }
    } else {
        Serial.println("Already connected to the server.");
        return true;
    }
}


void SmartBeehiveNetworkSingleton::Set_Server_Parameter(const char* serverAddress, uint16_t serverPort)
{
    if (serverAddress == nullptr || serverPort == 0) {
        Serial.println("Error: Invalid server address or port.");
        return;
    }

    this->serverAddress = serverAddress;
    this->serverPort = serverPort;
    Serial.println("Server parameters set successfully.");
}

void SmartBeehiveNetworkSingleton::Set_Wifi_Parameter(const char* ssid, const char* WIFI_password)
{
    if (ssid == nullptr || WIFI_password == nullptr) {
        Serial.println("Error: Invalid WiFi ssid or password.");
        return;
    }
    this->WiFi_ssid = ssid;
    this->WiFi_password = WIFI_password;
    Serial.println("WIFI parameters set successfully.");
}


// Method to get the WiFi client instance
// Returns the WiFiClient object for server communication
WiFiClient &SmartBeehiveNetworkSingleton::Get_Client()
{
    return this->WiFi_Client;
}

// Method to get the WiFi class instance
// Returns the WiFiClass object for WiFi management
WiFiClass &SmartBeehiveNetworkSingleton::Get_WiFi()
{
    return this->WIFI;
}

const char *SmartBeehiveNetworkSingleton::Get_WiFi_ssid() const
{
    return this->WiFi_ssid;
}
const char *SmartBeehiveNetworkSingleton::Get_WiFi_password() const
{
    return this->WiFi_password;
}
const char *SmartBeehiveNetworkSingleton::Get_serverAddress() const
{
    return this->serverAddress;
}
u16_t SmartBeehiveNetworkSingleton::Get_serverPort() const
{
    return this->serverPort;
}