#include "Smart_Beehive_Sensors.hpp"

Smart_Beehive_Sensors::Smart_Beehive_Sensors(const char* Sensor_ID, const char* Sensor_Endpoint)
    : Sensor_ID(Sensor_ID), Sensor_Endpoint(Sensor_Endpoint)
{
    // You can initialize the JSON document here if needed with default values
}

void Smart_Beehive_Sensors::Publish_Data_on_Server()
{
    Server_connect();

    if (WiFi_client.Get_Client().connected())
    {
        // serializeJson(this->json_data, Serial);
        Send_HTTP_POST_request(this->Sensor_Endpoint, this->json_data);
        Check_server_response();
        String server_response = Read_server_response();

        auto server_response_code = Get_response_code(server_response.c_str());
        Serial.println(server_response_code);

        if (server_response_code == HTTP_OK)
        {
            Serial.println("Data published successfully.");
        }
        else
        {
            Serial.print("Failed to publish data. HTTP status code: ");
            Serial.println(server_response_code);
        }
    }
    else
    {
        Serial.println("Failed to connect to server.");
    }
}
