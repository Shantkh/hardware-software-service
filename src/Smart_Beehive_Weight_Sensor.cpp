#include "Smart_Beehive_hardware_config.hpp"
#include "Smart_Beehive_Weight_Sensor.hpp"

Smart_Beehive_Weight_Sensor::Smart_Beehive_Weight_Sensor(
    const char *Sensor_ID,
    weight_sensor_pins_t Pins_config,
    const char *Server_Endpoint)
    : HX711(Pins_config.Clk_pin, Pins_config.Dout_pin), Smart_Beehive_Sensors(Sensor_ID, Server_Endpoint)
{
}
void Smart_Beehive_Weight_Sensor::Read_Sensor_data()
{
    default_API_t   create_json;
    this->weightRead=0;
    for (int i = 0; i < 10; i++)
        this->weightRead += HX711::read();

    this->json_data.clear();
    create_json.apiKey=CONFIG_API_KEY; //config file
    create_json.hiveId=CONFIG_HIVE_ID; //config file
    create_json.sensorData=std::to_string(this->weightRead);
    create_json.sensorEnum=CONFIG_SENSOR_ENUM;//config file --modify to hiveEnum
    create_json.sensorName=this->Sensor_ID; //const on the type of the sensor
    this->json_data = createJson(create_json);
}
