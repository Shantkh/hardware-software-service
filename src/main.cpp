#include <Arduino.h>
#include "Smart_Beehive_wifi_server_config.hpp"
#include "Smart_Beehive_hardware_config.hpp"
#include "Smart_Beehive_Temperature_Sensor.hpp"
#include "Smart_Beehive_Humidity_Sensor.hpp"
#include "Smart_Beehive_GPS_Sensor.hpp"
#include "Smart_Beehive_Mic_Sensor.hpp"
#include "Smart_Beehive_Weight_Sensor.hpp"
#include <cmath>

class Multiplier
{
  typedef union
  {
    struct
    {
      u8_t switch0 : 1;
      u8_t switch1 : 1;
      u8_t switch2 : 1;
      u8_t switch3 : 1;
      u8_t /*reserved*/ : 4
;
    };
    u8_t channel_number;
  } selectors_channel_t;

private:
  selectors_channel_t att_current_channel;
  Smart_Beehive_Sensors **att_ptr_channels_selected;
  uint8_t att_switches[4] = {0};

public:
  Multiplier(Smart_Beehive_Sensors **ptr_channels_selected, uint8_t SW0, uint8_t SW1, uint8_t SW2, uint8_t SW3);
  void Select_Channel(uint8_t channel);
};
// Temperate
Smart_Beehive_Temperature_Sensor temp_0("Temp_0", CONFIG_TEMP_SENSOR_PIN, CONFIG_TEMPERATURE_ENDPOINT);
Smart_Beehive_Temperature_Sensor temp_1("Temp_1", CONFIG_TEMP_SENSOR_PIN, CONFIG_TEMPERATURE_ENDPOINT);
Smart_Beehive_Temperature_Sensor temp_2("Temp_2", CONFIG_TEMP_SENSOR_PIN, CONFIG_TEMPERATURE_ENDPOINT);
Smart_Beehive_Temperature_Sensor temp_3("Temp_3", CONFIG_TEMP_SENSOR_PIN, CONFIG_TEMPERATURE_ENDPOINT);
Smart_Beehive_Temperature_Sensor temp_4("Temp_4", CONFIG_TEMP_SENSOR_PIN, CONFIG_TEMPERATURE_ENDPOINT);
Smart_Beehive_Temperature_Sensor temp_5("Temp_5", CONFIG_TEMP_SENSOR_PIN, CONFIG_TEMPERATURE_ENDPOINT);
Smart_Beehive_Temperature_Sensor temp_6("Temp_6", CONFIG_TEMP_SENSOR_PIN, CONFIG_TEMPERATURE_ENDPOINT);
Smart_Beehive_Temperature_Sensor temp_7("Temp_7", CONFIG_TEMP_SENSOR_PIN, CONFIG_TEMPERATURE_ENDPOINT);
Smart_Beehive_Temperature_Sensor temp_8("Temp_8", CONFIG_TEMP_SENSOR_PIN, CONFIG_TEMPERATURE_ENDPOINT);
Smart_Beehive_Sensors *ptr_temp_sensors[9] = {&temp_0, &temp_1, &temp_2, &temp_3, &temp_4, &temp_5, &temp_6, &temp_7, &temp_8};
Multiplier Temp_multiplier(ptr_temp_sensors, CONFIG_TEMP_MULTIPLEXER_SW0, CONFIG_TEMP_MULTIPLEXER_SW1, CONFIG_TEMP_MULTIPLEXER_SW2, CONFIG_TEMP_MULTIPLEXER_SW3);

// Humadity
Smart_Beehive_Humidity_Sensor Hum0("Hum_0", CONFIG_HUM_SENSOR_1_PIN, CONFIG_DHT_TYPE, CONFIG_HUMIDITY_ENDPOINT);
Smart_Beehive_Humidity_Sensor Hum1("Hum_1", CONFIG_HUM_SENSOR_2_PIN, CONFIG_DHT_TYPE, CONFIG_HUMIDITY_ENDPOINT);

// GPS
Smart_Beehive_GPS_Sensor GPS0("Gps0", Serial1, CONFIG_GPS_ENDPOINT);

// Microphone
Smart_Beehive_Mic_Sensor MIC0("Mic0", CONFIG_MICRO_SENSOR_PIN, CONFIG_MICROPHONE_ENDPOINT);
Smart_Beehive_Mic_Sensor MIC1("Mic1", CONFIG_MICRO_SENSOR_PIN, CONFIG_MICROPHONE_ENDPOINT);
Smart_Beehive_Mic_Sensor MIC2("Mic2", CONFIG_MICRO_SENSOR_PIN, CONFIG_MICROPHONE_ENDPOINT);
Smart_Beehive_Mic_Sensor MIC3("Mic3", CONFIG_MICRO_SENSOR_PIN, CONFIG_MICROPHONE_ENDPOINT);
Smart_Beehive_Mic_Sensor MIC4("Mic4", CONFIG_MICRO_SENSOR_PIN, CONFIG_MICROPHONE_ENDPOINT);
Smart_Beehive_Mic_Sensor MIC5("Mic5", CONFIG_MICRO_SENSOR_PIN, CONFIG_MICROPHONE_ENDPOINT);
Smart_Beehive_Mic_Sensor MIC6("Mic6", CONFIG_MICRO_SENSOR_PIN, CONFIG_MICROPHONE_ENDPOINT);
Smart_Beehive_Mic_Sensor MIC7("Mic7", CONFIG_MICRO_SENSOR_PIN, CONFIG_MICROPHONE_ENDPOINT);
Smart_Beehive_Mic_Sensor MIC8("Mic8", CONFIG_MICRO_SENSOR_PIN, CONFIG_MICROPHONE_ENDPOINT);
Smart_Beehive_Sensors *ptr_mic_sensors[9] = {&MIC0, &MIC1, &MIC2, &MIC3, &MIC4, &MIC5, &MIC6, &MIC7, &MIC8};
Multiplier Mic_multiplier(ptr_mic_sensors, CONFIG_MICRO_MULTIPLEXER_SW0, CONFIG_MICRO_MULTIPLEXER_SW1, CONFIG_MICRO_MULTIPLEXER_SW2, CONFIG_MICRO_MULTIPLEXER_SW3);

// Weight
Smart_Beehive_Weight_Sensor Weight0("Weight0", {.Clk_pin = CONFIG_WEIGHT_CLK, .Dout_pin = CONFIG_WEIGHT_SENSOR_PIN}, CONFIG_WEIGHT_ENDPOINT);
Smart_Beehive_Weight_Sensor Weight1("Weight1", {.Clk_pin = CONFIG_WEIGHT_CLK, .Dout_pin = CONFIG_WEIGHT_SENSOR_PIN}, CONFIG_WEIGHT_ENDPOINT);
Smart_Beehive_Weight_Sensor Weight2("Weight2", {.Clk_pin = CONFIG_WEIGHT_CLK, .Dout_pin = CONFIG_WEIGHT_SENSOR_PIN}, CONFIG_WEIGHT_ENDPOINT);
Smart_Beehive_Weight_Sensor Weight3("Weight3", {.Clk_pin = CONFIG_WEIGHT_CLK, .Dout_pin = CONFIG_WEIGHT_SENSOR_PIN}, CONFIG_WEIGHT_ENDPOINT);
Smart_Beehive_Weight_Sensor Weight4("Weight4", {.Clk_pin = CONFIG_WEIGHT_CLK, .Dout_pin = CONFIG_WEIGHT_SENSOR_PIN}, CONFIG_WEIGHT_ENDPOINT);
Smart_Beehive_Weight_Sensor Weight5("Weight5", {.Clk_pin = CONFIG_WEIGHT_CLK, .Dout_pin = CONFIG_WEIGHT_SENSOR_PIN}, CONFIG_WEIGHT_ENDPOINT);
Smart_Beehive_Weight_Sensor Weight6("Weight6", {.Clk_pin = CONFIG_WEIGHT_CLK, .Dout_pin = CONFIG_WEIGHT_SENSOR_PIN}, CONFIG_WEIGHT_ENDPOINT);
Smart_Beehive_Weight_Sensor Weight7("Weight7", {.Clk_pin = CONFIG_WEIGHT_CLK, .Dout_pin = CONFIG_WEIGHT_SENSOR_PIN}, CONFIG_WEIGHT_ENDPOINT);
Smart_Beehive_Weight_Sensor Weight8("Weight8", {.Clk_pin = CONFIG_WEIGHT_CLK, .Dout_pin = CONFIG_WEIGHT_SENSOR_PIN}, CONFIG_WEIGHT_ENDPOINT);
Smart_Beehive_Sensors *ptr_weigh_sensors[9] = {&Weight0, &Weight1, &Weight2, &Weight3, &Weight4, &Weight5, &Weight6, &Weight7, &Weight8};
Multiplier Weight_multiplier(ptr_weigh_sensors, CONFIG_WEIGHT_MULTIPLEXER_SW0, CONFIG_WEIGHT_MULTIPLEXER_SW1, CONFIG_WEIGHT_MULTIPLEXER_SW2, CONFIG_WEIGHT_MULTIPLEXER_SW3);

void setup()
{
  Serial.begin(115200);
  Smart_Beehive_Client client_(CONFIG_WIFI_SSID, CONFIG_WIFI_PASSWORD, CONFIG_SERVER_ADDRESS, CONFIG_SERVER_PORT);
}
uint8_t current_channel = 0;

void loop()
{
  Temp_multiplier.Select_Channel(current_channel);
  Mic_multiplier.Select_Channel(current_channel);
  Weight_multiplier.Select_Channel(current_channel);
  current_channel++;
  
  // Humadity
  Hum0.Read_Sensor_data();
  Hum0.Publish_Data_on_Server();
  Hum1.Read_Sensor_data();
  Hum1.Publish_Data_on_Server();
  
  GPS0.Read_Sensor_data();
  GPS0.Publish_Data_on_Server();
  
  if (current_channel > 8)
  {
    current_channel = 0;
  }
}

Multiplier::Multiplier(Smart_Beehive_Sensors **ptr_channels_selected, uint8_t SW0, uint8_t SW1, uint8_t SW2, uint8_t SW3)
    : att_ptr_channels_selected(ptr_channels_selected), att_switches{SW0, SW1, SW2, SW3}
{
  pinMode(SW0,OUTPUT);
  pinMode(SW1,OUTPUT);
  pinMode(SW2,OUTPUT);
  pinMode(SW3,OUTPUT);
  att_current_channel.channel_number = 0;
}

void Multiplier::Select_Channel(uint8_t channel)
{
  att_current_channel.channel_number = channel;
  digitalWrite(att_switches[0], att_current_channel.switch0 == 1 ? HIGH : LOW);
  digitalWrite(att_switches[1], att_current_channel.switch1== 1 ? HIGH : LOW);
  digitalWrite(att_switches[2], att_current_channel.switch2== 1 ? HIGH : LOW);
  digitalWrite(att_switches[3], att_current_channel.switch3== 1 ? HIGH : LOW);
  att_ptr_channels_selected[att_current_channel.channel_number]->Read_Sensor_data();
  att_ptr_channels_selected[att_current_channel.channel_number]->Publish_Data_on_Server();
}
