#include "Smart_Beehive_Client.hpp"
#include <string>

/*****************************************************************************************/



/*****************************************************************************************/
Smart_Beehive_Client::Smart_Beehive_Client(const char *WIFI_ssid, const char *WIFI_password, const char *server_Address, u16_t server_Port)
{
    if (WIFI_ssid == nullptr || WIFI_password == nullptr || server_Address == nullptr || server_Port == 0) {
        Serial.println("Error: Invalid initialization parameters.");
        return;
    }

    if (!this->WiFi_client.Connect_Wifi(WIFI_ssid, WIFI_password)) {
        Serial.println("Error: Failed to connect to WiFi.");
    }

    if (!this->WiFi_client.Connect_Server(server_Address, server_Port)) {
        Serial.println("Error: Failed to connect to server.");
    }

    this->WiFi_client.Set_Server_Parameter(server_Address,server_Port);

    this->WiFi_client.Set_Wifi_Parameter(WIFI_ssid,WIFI_password);
}

Smart_Beehive_Client::Smart_Beehive_Client() {}




StaticJsonDocument<JSON_BUFFER_SIZE> Smart_Beehive_Client::createJson(default_API_t API_Data)
{
    StaticJsonDocument<JSON_BUFFER_SIZE> jsonDocument;
    #define member(type, name) jsonDocument[#name] = API_Data.name;
    default_Json_data
    #undef member
    return jsonDocument;
}

bool Smart_Beehive_Client::Send_HTTP_POST_request(String endpoint_url, JsonDocument &send_jsonData)
{
    const char * serverAddress=this->WiFi_client.Get_serverAddress();
    uint16_t serverPort=this->WiFi_client.Get_serverPort();
    // Check if the client is connected
    if (this->WiFi_client.Get_Client().connected()) {
        char buffer[JSON_BUFFER_SIZE];
        serializeJson(send_jsonData, buffer);
        String send_Data = String(buffer);
        String request = String("POST ") + endpoint_url + " HTTP/1.1\r\n" + \
                        "Host: " + String(serverAddress) + ":" + String(serverPort) + "\r\n" + \
                        "Content-Type: application/json\r\n" + \
                        "Content-Length: " + String(send_Data.length()) + "\r\n" + \
                        "Connection: close\r\n\r\n" + \
                        send_Data;
        // Serial.println(request);
        // Send the request
        this->WiFi_client.Get_Client().print(request);
        // Optionally check if the request was sent or if there were issues
        if (this->WiFi_client.Get_Client().connected()) {
            return true;
        } else {
            Serial.println("Error: Failed to send request.");
            return false;
        }
    } else {
        Serial.println("Error: Server is not Connected.");
        return false;
    }
}


bool Smart_Beehive_Client::Send_HTTP_GET_request(String endpoint_url)
{
    const char * serverAddress=this->WiFi_client.Get_serverAddress();
    uint16_t serverPort=this->WiFi_client.Get_serverPort();

    if (this->WiFi_client.Get_Client().connected()) {
        this->WiFi_client.Get_Client().print(
            String("GET ") + endpoint_url + " HTTP/1.1\r\n" +
            "Host: " + String(serverAddress) + ":" + String(serverPort) + "\r\n" + \
            "Connection: close\r\n\r\n");
        return true;
    } else {
        Serial.println("Error: Not connected to the server.");
        return false;
    }
}

void Smart_Beehive_Client::Check_server_response()
{
    Serial.println("Wait for server response");
    while (this->WiFi_client.Get_Client().connected() && !this->WiFi_client.Get_Client().available()) {
        
        delay(1);
    }
    Serial.println("Get the for server response");
}

String Smart_Beehive_Client::Read_server_response()
{
    String server_response;
    
    while (this->WiFi_client.Get_Client().available()) {
        String response = this->WiFi_client.Get_Client().readStringUntil('\r');
        server_response += response;
    }
    return server_response;
}

HTTP_Status_t Smart_Beehive_Client::Get_response_code(const char *response)
{
    std::string __response = response;
    int headerEnd = __response.find("Date");
    HTTP_Status_t statusCode = HTTP_Error;

    if (headerEnd != -1) {
        std::string statusLine = __response.substr(0, headerEnd);
        int firstSpaceIndex = statusLine.find(' ');
        int secondSpaceIndex = statusLine.find(' ', firstSpaceIndex + 1);

        if (firstSpaceIndex != -1 && secondSpaceIndex != -1) {
            std::string statusCodeStr = statusLine.substr(firstSpaceIndex + 1, secondSpaceIndex - firstSpaceIndex);
            int statusCode_ = std::stoi(statusCodeStr);
            statusCode = (HTTP_Status_t)statusCode_;
        }
    }
    return statusCode;
}

bool Smart_Beehive_Client::Server_connect()
{
    if (!this->WiFi_client.Connect_Server()) {
        Serial.println("Error: Failed to connect to the server.");
        return false;
    }
    return true;
}