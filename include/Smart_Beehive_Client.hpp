#ifndef SMART_BEEHIVE_CLIENT_H
#define SMART_BEEHIVE_CLIENT_H

#include <Arduino.h>
#include <ArduinoJson.h>
#include "Smart_Beehive_wifi_server_config.hpp"
#include"Smart_Beehive_API.hpp"
#include "SmartBeehiveNetworkSingleton.hpp"
#define JSON_BUFFER_SIZE 1024

typedef enum {
    // Informational responses (100–199)
    HTTP_Continue = 100,
    HTTP_SwitchingProtocols = 101,
    HTTP_Processing = 102,
    HTTP_EarlyHints = 103,

    // Successful responses (200–299)
    HTTP_OK = 200,
    HTTP_Created = 201,
    HTTP_Accepted = 202,
    HTTP_NonAuthoritativeInformation = 203,
    HTTP_NoContent = 204,
    HTTP_ResetContent = 205,
    HTTP_PartialContent = 206,
    HTTP_MultiStatus = 207,
    HTTP_AlreadyReported = 208,
    HTTP_IMUsed = 226,

    // Redirection messages (300–399)
    HTTP_MultipleChoices = 300,
    HTTP_MovedPermanently = 301,
    HTTP_Found = 302,
    HTTP_SeeOther = 303,
    HTTP_NotModified = 304,
    HTTP_UseProxy = 305,
    HTTP_TemporaryRedirect = 307,
    HTTP_PermanentRedirect = 308,

    // Client error responses (400–499)
    HTTP_BadRequest = 400,
    HTTP_Unauthorized = 401,
    HTTP_PaymentRequired = 402,
    HTTP_Forbidden = 403,
    HTTP_NotFound = 404,
    HTTP_MethodNotAllowed = 405,
    HTTP_NotAcceptable = 406,
    HTTP_ProxyAuthenticationRequired = 407,
    HTTP_RequestTimeout = 408,
    HTTP_Conflict = 409,
    HTTP_Gone = 410,
    HTTP_LengthRequired = 411,
    HTTP_PreconditionFailed = 412,
    HTTP_PayloadTooLarge = 413,
    HTTP_URITooLong = 414,
    HTTP_UnsupportedMediaType = 415,
    HTTP_RangeNotSatisfiable = 416,
    HTTP_ExpectationFailed = 417,
    HTTP_MisdirectedRequest = 421,
    HTTP_UnprocessableEntity = 422,
    HTTP_Locked = 423,
    HTTP_FailedDependency = 424,
    HTTP_TooEarly = 425,
    HTTP_UpgradeRequired = 426,
    HTTP_PreconditionRequired = 428,
    HTTP_TooManyRequests = 429,
    HTTP_RequestHeaderFieldsTooLarge = 431,
    HTTP_UnavailableForLegalReasons = 451,

    // Server error responses (500–599)
    HTTP_InternalServerError = 500,
    HTTP_NotImplemented = 501,
    HTTP_BadGateway = 502,
    HTTP_ServiceUnavailable = 503,
    HTTP_GatewayTimeout = 504,
    HTTP_HTTPVersionNotSupported = 505,
    HTTP_VariantAlsoNegotiates = 506,
    HTTP_InsufficientStorage = 507,
    HTTP_LoopDetected = 508,
    HTTP_NotExtended = 510,
    HTTP_NetworkAuthenticationRequired = 511,

    // Custom error responses
    HTTP_Error = 700
} HTTP_Status_t;





/**
 * @brief Smart_Beehive_Client class for managing communication with a server over WiFi.
 *
 * This class inherits from WiFiClient to leverage its networking capabilities.
 */
class Smart_Beehive_Client {
private:
    IPAddress IP;
    SmartBeehiveNetworkSingleton &WiFi_client = SmartBeehiveNetworkSingleton::Get_Instance();

public:
    Smart_Beehive_Client(const char *WIFI_ssid, const char *WIFI_password, const char *server_Address, u16_t server_Port);
    Smart_Beehive_Client();

    StaticJsonDocument<JSON_BUFFER_SIZE> createJson(default_API_t API_Data);

    bool Send_HTTP_POST_request(String endpoint_url, JsonDocument &send_jsonData);
    bool Send_HTTP_GET_request(String endpoint_url);

    void Check_server_response();
    String Read_server_response();

    HTTP_Status_t Get_response_code(const char* response);
    bool Server_connect();
};

#endif // SMART_BEEHIVE_CLIENT_H
