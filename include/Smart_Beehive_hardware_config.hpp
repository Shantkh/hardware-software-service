#ifndef HARDWARE_CONFIG_H
#define HARDWARE_CONFIG_H

/*
 * Hardware Pins Configuration
 *
 * This section defines the GPIO pins used for connecting sensors to the ESP32.
 * Ensure that the pins are correctly connected to the respective sensors.
 */
/********************************************************************************************************/
/*________________________________________   SENSORS   ________________________________________________*/
/*_____________________________________________________________________________________________________*/
/********************************************************************************************************/
/* This section maps the GPIO pins to the respective sensors in the system. 
 * These pins are essential for reading data from the sensors, and multiplexers are used 
 * where necessary to manage multiple sensors using fewer pins.
 */
/*-----------------------------------------------------------------------------------------*/ 
            /*                          Temperature Sensor                      */
/*-----------------------------------------------------------------------------------------*/ 
// Pin used for the temperature sensor
#define CONFIG_TEMP_SENSOR_PIN              39 
// Pins used for controlling the temperature sensor's multiplexer switches
#define CONFIG_TEMP_MULTIPLEXER_SW0         19
#define CONFIG_TEMP_MULTIPLEXER_SW1         18
#define CONFIG_TEMP_MULTIPLEXER_SW2         5
#define CONFIG_TEMP_MULTIPLEXER_SW3         17
/*-----------------------------------------------------------------------------------------*/ 
            /*                          Smoke Sensor                      */
/*-----------------------------------------------------------------------------------------*/ 
// Pin used for the smoke sensor
#define CONFIG_SMOKE_SENSOR_PIN              36 
/*-----------------------------------------------------------------------------------------*/ 
            /*                          Weight Sensor                      */
/*-----------------------------------------------------------------------------------------*/ 
// Pin used for the weight sensor (load cell)
#define CONFIG_WEIGHT_SENSOR_PIN              34 

// Pins used for controlling the weight sensor's multiplexer switches
#define CONFIG_WEIGHT_MULTIPLEXER_SW0         16
#define CONFIG_WEIGHT_MULTIPLEXER_SW1         4
#define CONFIG_WEIGHT_MULTIPLEXER_SW2         2
#define CONFIG_WEIGHT_MULTIPLEXER_SW3         15

// Clock pin for the weight sensor
#define CONFIG_WEIGHT_CLK                     0
/*-----------------------------------------------------------------------------------------*/ 
            /*                          Microphone Sensor                      */
/*-----------------------------------------------------------------------------------------*/ 
// Pin used for the microphone sensor
#define CONFIG_MICRO_SENSOR_PIN              35 

// Pins used for controlling the microphone sensor's multiplexer switches
#define CONFIG_MICRO_MULTIPLEXER_SW0         25
#define CONFIG_MICRO_MULTIPLEXER_SW1         26
#define CONFIG_MICRO_MULTIPLEXER_SW2         27
#define CONFIG_MICRO_MULTIPLEXER_SW3         14
/*-----------------------------------------------------------------------------------------*/ 
            /*                          HUMIDITY Sensor                      */
/*-----------------------------------------------------------------------------------------*/ 
// Pins used for the DHT humidity sensors
#define CONFIG_HUM_SENSOR_1_PIN            32
#define CONFIG_HUM_SENSOR_2_PIN            33

// DHT sensor type (can be one of several options, such as DHT11, DHT22)
#define CONFIG_DHT_TYPE                    DHT11  // Possible values: DHT11, DHT12, DHT21, DHT22, AM2301
/********************************************************************************************************/
/*________________________________________ SENSORS END ________________________________________________*/
/*_____________________________________________________________________________________________________*/
/********************************************************************************************************/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/********************************************************************************************************/
/*________________________________________  ACTUATORS  ________________________________________________*/
/*_____________________________________________________________________________________________________*/
/********************************************************************************************************/
/* This section maps GPIO pins to the actuators in the system, 
 * which includes fans, a buzzer, and an LED for system notifications.
 */

/*-----------------------------------------------------------------------------------------*/ 
            /*                          Actuators                      */
/*-----------------------------------------------------------------------------------------*/ 

// Pins used for controlling the fans
#define CONFIG_FAN_1                12  
#define CONFIG_FAN_2                13

// Pin used for controlling the buzzer
#define CONFIG_BUZZER               14

// Pin used for controlling the LED
#define CONFIG_LED                  23
/********************************************************************************************************/
/*_______________________________________ ACTUATORS END _______________________________________________*/
/*_____________________________________________________________________________________________________*/
/********************************************************************************************************/


#endif  // HARDWARE_CONFIG_H