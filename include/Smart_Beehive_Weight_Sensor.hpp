#ifndef SMART_BEEHIVE_Weight_SENSOR_H
#define SMART_BEEHIVE_Weight_SENSOR_H
#include "Smart_Beehive_Sensors.hpp"
#include "HX711.h"



typedef struct 
{ 
    uint8_t Clk_pin;
    uint8_t Dout_pin; 
}weight_sensor_pins_t;


class Smart_Beehive_Weight_Sensor : public HX711, public Smart_Beehive_Sensors
{
private:
    float_t weightRead;
public:
    Smart_Beehive_Weight_Sensor(
        const char *Sensor_ID,
        weight_sensor_pins_t Pins_config,
        const char *Server_Endpoint);
    void Read_Sensor_data() override;
};

#endif