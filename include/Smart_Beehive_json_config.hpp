#ifndef JSON_CONFIG_H
#define JSON_CONFIG_H

/*
 * JSON Configuration Values
 *
 * This section contains the configuration values that will be sent to the server 
 * as part of the JSON payload. Update these values as needed to match your 
 * server's requirements.
 */

#define CONFIG_API_KEY                  "c79a0d0b-66a0-43e7-9b3e-d0a550032d7b"  // API key for authentication
#define CONFIG_HIVE_ID                  "e7b7fbdc-8a91-4c40-bd63-4cc8d55dbf8b"  // Hive ID for the sensor
#define CONFIG_SENSOR_ENUM              "HIVE_FLOOR_THREE"  // Sensor enum indicating the sensor type or location

/*
 * JSON Data Structure
 *
 * This section defines the structure of the JSON data that will be sent to the server.
 * It maps the required fields to their respective data types.
 */

#define default_Json_data   member(std::string, apiKey)\
                            member(std::string, sensorName)\
                            member(std::string, hiveId)\
                            member(std::string, sensorEnum)\
                            member(std::string, sensorData)



#define Humanity_Json_data  member(std::string, apiKey)\
                            member(std::string, sensorName)\
                            member(std::string, hiveId)\
                            member(std::string, sensorEnum)\
                            member(std::string, sensorData)


#define GPS_Json_data       member(std::string, apiKey)\
                            member(std::string, sensorName)\
                            member(std::string, hiveId)\
                            member(std::string, sensorEnum)\
                            member(std::string, locationLatitude)\
                            member(std::string, locationLongitude)\
                            member(std::string, locationLatitudeDegree)\
                            member(std::string, locationLongitudeDegree)\
                            member(std::string, gpsAngle)\
                            member(std::string, gpsAltitude)
/*
 * Default Sensors Values
 *
 * This section defines the default values that should be used if a sensor 
 * fails to send data. These values can be used as fallback to ensure that 
 * the system continues to operate with some data rather than none.
 */

#define CONFIG_DEFAULT_SENSOR_VALUE 0.1  // Default sensor value when no data is received


#endif  // JSON_CONFIG_H
