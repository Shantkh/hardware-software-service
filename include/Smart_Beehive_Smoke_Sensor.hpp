#ifndef SMART_BEEHIVE_SMOKE_SENSOR_H
#define SMART_BEEHIVE_SMOKE_SENSOR_H

#include "Smart_Beehive_Sensors.hpp"

class Smart_Beehive_Smoke_Sensor : public Smart_Beehive_Sensors
{
private:
    int sensorPin;  // Pin for the smoke sensor
    int smokeLevel; // To store the sensor reading
public:
    Smart_Beehive_Smoke_Sensor(const char* Sensor_ID, int Sensor_Pin, const char* Server_Endpoint);
    void Read_Sensor_data() override;  // Function to read smoke sensor data
};

#endif
