#ifndef SMART_BEEHIVE_SENSORS_H
#define SMART_BEEHIVE_SENSORS_H

#include"Smart_Beehive_Client.hpp"


class Smart_Beehive_Sensors : public Smart_Beehive_Client
{
protected:
    const char* Sensor_ID;
    const char* Sensor_Endpoint;
    StaticJsonDocument<JSON_BUFFER_SIZE> json_data;
    SmartBeehiveNetworkSingleton &WiFi_client = SmartBeehiveNetworkSingleton::Get_Instance();

public:
    Smart_Beehive_Sensors(const char* Sensor_ID, const char* Sensor_Endpoint);
    void Publish_Data_on_Server();
    virtual void Read_Sensor_data() = 0;
};

#endif