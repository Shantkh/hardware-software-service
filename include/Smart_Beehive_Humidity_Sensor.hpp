#ifndef SMART_BEEHIVE_HUMIDITY_SENSOR_H
#define SMART_BEEHIVE_HUMIDITY_SENSOR_H

#include "Smart_Beehive_Sensors.hpp"
#include <DHT.h>

class Smart_Beehive_Humidity_Sensor : public Smart_Beehive_Sensors, DHT
{
private:
    float_t HumidityRead;
public:
    Smart_Beehive_Humidity_Sensor(const char* Sensor_ID, uint16_t DHT_PIN, uint8_t DHT_TYPE,const char* Server_Endpoint);
    void Read_Sensor_data() override;
};

#endif