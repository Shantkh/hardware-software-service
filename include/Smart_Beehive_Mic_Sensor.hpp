#ifndef SMART_BEEHIVE_MIC_SENSOR_H
#define SMART_BEEHIVE_MIC_SENSOR_H

#include "Smart_Beehive_Sensors.hpp"

class Smart_Beehive_Mic_Sensor : public Smart_Beehive_Sensors
{
private:
    float dB_Level;      // Store the calculated decibel level
    int sensorPin;       // Pin to read the analog signal from the microphone
public:
    Smart_Beehive_Mic_Sensor(const char* Sensor_ID, int Sensor_Pin, const char* Server_Endpoint);
    void Read_Sensor_data() override;
};

#endif