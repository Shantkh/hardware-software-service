#ifndef SMART_BEEHIVE_CLIENT_SINGLETON_H
#define SMART_BEEHIVE_CLIENT_SINGLETON_H

#include <WiFi.h>
#include <WiFiClient.h>

class SmartBeehiveNetworkSingleton
{
private:
    WiFiClass WIFI;
    WiFiClient WiFi_Client;
    const char* WiFi_ssid = nullptr;
    const char* WiFi_password = nullptr;
    const char* serverAddress = nullptr;
    uint16_t serverPort = 0;
    
    SmartBeehiveNetworkSingleton();
    SmartBeehiveNetworkSingleton(SmartBeehiveNetworkSingleton &) = delete;
    SmartBeehiveNetworkSingleton &operator=(SmartBeehiveNetworkSingleton &) = delete;

public:
    static SmartBeehiveNetworkSingleton& Get_Instance();

    bool Connect_Wifi(const char* ssid, const char* WIFI_password);
    bool Connect_Server(const char* serverAddress, uint16_t serverPort);
    bool Connect_Server();
    void Set_Server_Parameter(const char* serverAddress, uint16_t serverPort);
    void Set_Wifi_Parameter(const char* ssid, const char* WIFI_password);

    WiFiClient& Get_Client();
    WiFiClass& Get_WiFi();
    const char* Get_WiFi_ssid() const;
    const char* Get_WiFi_password() const;
    const char* Get_serverAddress() const;
    uint16_t Get_serverPort() const;
};

#endif // SMART_BEEHIVE_CLIENT_SINGLETON_H
