#ifndef SMART_BEEHIVE_GPS_SENSOR_H
#define SMART_BEEHIVE_GPS_SENSOR_H

#include "Smart_Beehive_Sensors.hpp"
#include <TinyGPSPlus.h>

class Smart_Beehive_GPS_Sensor : public Smart_Beehive_Sensors
{
private:
    TinyGPSPlus gps;          // GPS object for handling GPS data
    HardwareSerial &gpsSerial; // Reference to the hardware serial port for GPS communication
    float latitude;            // Store the latitude value
    float longitude;           // Store the longitude value
public:
    Smart_Beehive_GPS_Sensor(const char* Sensor_ID, HardwareSerial &serialPort, const char* Server_Endpoint);
    void Read_Sensor_data() override;  // Function to read GPS data
    StaticJsonDocument<JSON_BUFFER_SIZE>  createJson(GPS_API_t API_Data);
};

#endif
