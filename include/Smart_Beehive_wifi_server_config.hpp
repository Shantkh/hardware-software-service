#ifndef WIFI_SERVER_CONFIG_H
#define WIFI_SERVER_CONFIG_H

/*
 * Wi-Fi and Server Configuration
 *
 * This section contains configuration values for Wi-Fi connectivity and 
 * server communication. Update these values with your network credentials, 
 * server details, and the specific API endpoint.
 */

#define CONFIG_WIFI_SSID            "B_C_D"    // Wi-Fi SSID
#define CONFIG_WIFI_PASSWORD        "MohamedAhmed2290998@#$"     // Wi-Fi Password
#define CONFIG_SERVER_ADDRESS       "n8k0csw4cs8gw8wkwcswsk8k.95.140.192.45.sslip.io"  // Server Address
#define CONFIG_SERVER_PORT           9002            // Server Port
#define CONFIG_TEMPERATURE_ENDPOINT       "/api/v1/heatSensor"  // Server API Endpoint
#define CONFIG_HUMIDITY_ENDPOINT          "/api/v1/heatSensor"  // Server API Endpoint
#define CONFIG_MICROPHONE_ENDPOINT        "/api/v1/microphone"  // Server API Endpoint
#define CONFIG_WEIGHT_ENDPOINT            "/api/v1/weight"  // Server API Endpoint
#define CONFIG_GPS_ENDPOINT               "/api/v1/location"  // Server API Endpoint


#endif  // WIFI_SERVER_CONFIG_H
