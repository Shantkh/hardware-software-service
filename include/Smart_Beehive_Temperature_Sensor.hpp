#ifndef SMART_BEEHIVE_Temperature_SENSOR_H
#define SMART_BEEHIVE_Temperature_SENSOR_H

#include "Smart_Beehive_Sensors.hpp"
#include <OneWire.h>
#include <DallasTemperature.h>
#include<DHT.h>

typedef enum
{
    DHT_Sensor=0,
    Dallas_Sensor
}Temperature_type_t;



class Smart_Beehive_Temperature_Sensor : public Smart_Beehive_Sensors, DallasTemperature,DHT
{
private:
    float_t temperatureRead;
    OneWire oneWire;
    Temperature_type_t Sensor_type;
public:
    Smart_Beehive_Temperature_Sensor(const char* Sensor_ID,
                                    uint16_t One_wire_bus_pin,
                                    const char* Server_Endpoint);
    Smart_Beehive_Temperature_Sensor(const char* Sensor_ID,
                                    uint16_t DHT_PIN,
                                    uint8_t DHT_TYPE,
                                    const char* Server_Endpoint);

    void Read_Sensor_data() override;
};

#endif