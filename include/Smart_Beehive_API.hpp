#ifndef SMART_BEEHIVE_API_H
#define SMART_BEEHIVE_API_H

#include"Smart_Beehive_json_config.hpp"
typedef struct
{
    #define member(type, name) type name;
    default_Json_data
    #undef member
}default_API_t;



typedef struct
{
    #define member(type, name) type name;
    GPS_Json_data
    #undef member
}GPS_API_t;

#endif