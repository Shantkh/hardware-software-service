-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 29, 2024 at 05:18 PM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Sensors`
--

-- --------------------------------------------------------

--
-- Table structure for table `Humidity_Sensor`
--

CREATE TABLE `Humidity_Sensor` (
  `Time` datetime NOT NULL DEFAULT current_timestamp(),
  `Sensor_ID` int(11) NOT NULL,
  `Sensor_value` float NOT NULL,
  `unit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Humidity_Sensor`
--

INSERT INTO `Humidity_Sensor` (`Time`, `Sensor_ID`, `Sensor_value`, `unit`) VALUES
('2024-07-11 08:35:30', 2, 0, '%'),
('2024-07-11 08:37:14', 2, 0, '%'),
('2024-07-11 08:38:58', 2, 0, '%');

-- --------------------------------------------------------

--
-- Table structure for table `Temperature_Sensor`
--

CREATE TABLE `Temperature_Sensor` (
  `Time` datetime NOT NULL DEFAULT current_timestamp(),
  `Sensor_ID` int(11) NOT NULL,
  `Sensor_value` float NOT NULL,
  `unit` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `Temperature_Sensor`
--

INSERT INTO `Temperature_Sensor` (`Time`, `Sensor_ID`, `Sensor_value`, `unit`) VALUES
('2024-07-11 08:35:07', 0, 0, '°C'),
('2024-07-11 08:35:08', 1, 0, '°C'),
('2024-07-11 08:35:28', 0, 0, '°C'),
('2024-07-11 08:35:29', 1, 0, '°C'),
('2024-07-11 08:37:12', 0, 0, '°C'),
('2024-07-11 08:37:13', 1, 0, '°C'),
('2024-07-11 08:38:56', 0, 0, '°C'),
('2024-07-11 08:38:57', 1, 0, '°C');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Humidity_Sensor`
--
ALTER TABLE `Humidity_Sensor`
  ADD UNIQUE KEY `Time` (`Time`) USING BTREE;

--
-- Indexes for table `Temperature_Sensor`
--
ALTER TABLE `Temperature_Sensor`
  ADD UNIQUE KEY `Time` (`Time`) USING BTREE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
